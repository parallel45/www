import React from 'react';
import Layout from '../components/layout';

const NotFoundPage = () => (
    <Layout title="Not found">
        <p>Sorry, nothing here.</p>
    </Layout>
);

export default NotFoundPage;
